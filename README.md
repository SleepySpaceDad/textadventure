# Mars Game
This is my final project for my GAME101 class at GMU. It's a fully-featured text adventure game. Through this project, I learned how to save and load games using the pickle library. It's a silly game about an astronaut who becomes stranded on Mars and needs to reestablish communications with Mission Control. I created a custom word-processing system that can take verb phrases and nouns which can then be processed by the game code. 

