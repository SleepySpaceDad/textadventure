import input
import os
import pickle
import string 
from enum import Enum

title = 'MarsGame'
save_path = os.path.expanduser('~') + os.path.sep + title
if(not os.path.exists(save_path)):
    os.makedirs(save_path)
save_file = open(save_path + os.path.sep + 'save.dat', 'w+b')

class Location(Enum):
    BEDROOM = 0
    HUB = 1
    OUTSIDE = 2
    ROVER = 3

class FoodState(Enum):
    NOTHING = 0
    MADE = 1
    EATEN = 2

class RoverState(Enum):
    NotFound = 0
    Found = 1
    Pushing = 2
    InHAB = 3


MARTIAN_ARRIVAL = 5

class Save:
    def __init__(self):
        self.started = False
        self.location = Location.BEDROOM
        self.bed_made = False
        self.turns = 0
        self.food = FoodState.NOTHING
        self.door_open = False
        self.airlock_open = False
        self.eva_suit = False
        self.book = False
        self.alien_dialogue = False
        self.photo = False
        self.need_photo = False
        self.rover_state = RoverState.NotFound
        self.victory = False

save = Save()

def open_airlock():
    if(not save.airlock_open):
        print('You press the button to the right of the airlock, and the door swings open.')
        save.airlock_open = True
    else:
        print('You cannot open an already opened airlock.')

def go_through_airlock():
    if(save.airlock_open):
        if(save.eva_suit):
            msg = 'You step inside the open airlock, and you press the button on the far end of the small tunnel. The door behind '\
                'you closes, and the door in front of you swings open. '
            if(save.location == Location.OUTSIDE):
                save.location = Location.HUB
                if(save.rover_state == RoverState.Pushing):
                    msg = 'You push the rover into the airlock. You carefully manuever around the rover to press the button on the far ' \
                    'end of the tunnel. The door behind you closes, and the door in front of you swings open. You push the rover into the ' \
                    'now open hub. '
                    save.rover_state = RoverState.InHAB
                if(save.need_photo):
                    msg += 'You see the photobook near the terminals. That must have a photo of the Curiosity Rover! '
                msg += 'A gush of air enters the airlock. '
            elif(save.location == Location.HUB):
                save.location = Location.OUTSIDE
                msg += 'A gush of air exits the airlock into the low-pressure martian atmosphere. '
                if(save.turns >= MARTIAN_ARRIVAL):
                    msg += 'You see an unfamiliar organism outside of your HAB. It waves a tentacle in greeting. '
            print(msg)
        else:
            print('You cannot go through the airlock without your EVA-suit.')
    else:
        print('You cannot go through a closed airlock.')

def translate_to_martian(eng):
    alphabet = string.ascii_lowercase + string.ascii_uppercase
    
    reverse_alphabet = ''
    for c in string.ascii_uppercase:
        reverse_alphabet = c + reverse_alphabet
    for c in string.ascii_lowercase:
        reverse_alphabet = c + reverse_alphabet
    
    table = eng.maketrans(alphabet, reverse_alphabet)
    
    return eng.translate(table)
    

def talk_to_martian():
    if(not save.book):
        print('You attempt to talk to the alien, but it only gives you a blank stare from its three eyes. You hear it ' \
            'say' + translate_to_martian('Hello, how are you.') + 'You remember that you forgot to actually learn the ' \
            'Martian language. You were too caught up in the other preparations for your mission to learn an entirely '\
            'foreign language. Fortunately, you have The Martian Language for Dummies in your room. You realize that '\
            'the book is aptly named.')
    else:
        if(not save.alien_dialogue):
            print('You hear the alien say ' + translate_to_martian('Hello, who are you') + '?" Translated, that means "Hello, ' \
                'who are you?"')
            save.alien_dialogue = True
            
        print('With the book, you are able to communicate with the alien. You wait a moment as you decide what to say to the alien.')
        dialogue = input.parse_dialogue()
        
        print('You carefully translate your words into Martian with the aid of your book. ' \
            'You say "' + translate_to_martian(dialogue).capitalize() + '."')

        if(dialogue == 'hello' or dialogue == 'hi' or dialogue == 'greetings' or dialogue == 'howdy'):
            print( 'The alien waves one of its tentacles at you. You think it\'s smiling, but you aren\'t sure.')
        elif(dialogue == 'how are you'):
            print('The alien responds by saying, "' + translate_to_martian('Good') + '." That\'s Martian for "Good."')
        elif(dialogue.startswith('my name is ')):
            name = dialogue[11:]
            print('The alien says "' + translate_to_martian('Hello, ' + name) +'."')
        elif(dialogue == 'can you help me find the rover' or dialogue == 'where is the rover' or dialogue == 'where is the curiosity rover' or \
            dialogue == 'can you help me find the curiosity rover' or (dialogue == 'where is this' and save.photo)):
            if(save.photo):
                print('You take the photo of the rover out of your pocket and show it to alien as you speak. The alien\'s face lights up in ' \
                'recognition of the object in the photo. It happily says, "' + translate_to_martian('It\'s not far from here') +'!" The rover ' \
                'isn\'t far from your current location. It gestures to the right with a couple of its tentacles.')
                save.rover_state = RoverState.Found
            else:
                print('The alien blinks its three eyes for a moment before saying, "'+ translate_to_martian('What')+ '?" ' \
                    'That was martian for "What?" It doesn\'t seem to know what the Curiosity Rover is or what it looks like. ' \
                    'Perhaps you could find a picture of the rover looks like and show it to the alien.')
                save.need_photo = True
        elif(dialogue == 'where are we' or dialogue == 'where am i' or dialogue == 'where are you'):
            print('The alien says, "' + translate_to_martian('Mars') +'." The two of you are on Mars.')
        elif(dialogue == 'who are you'):
            print('The alien says,"' + translate_to_martian('I am Karl') + '." So, the alien does have a name. You can\'t translate seem ' \
                'to be able to translate it to English, however.')        
        else:
            print('The alien stares at you blankly. Perhaps your Martian was a bit off. You are, after all, blindly translating it from a ' \
                'a book without any knowledge of the language.')

while(True):
    if(not save.started):
        print('You wake up and go to your terminal. The screen reads "CONNECTION LOST". ' \
            'You are stranded on Mars without any help from Mission Control. You need to ' \
            'reestablish the conection, else face the cruel Martian world without any hope ' \
            'for receiving the resupply. You have one month to reestablish communication ' \
            'before you\'ll run out of food and starve to death. You remember that the Curiosity ' \
            'Rover has a communications array that you can take advatange of, if only you could ' \
            'find it.')
        save.started = True
    elif(save.turns == MARTIAN_ARRIVAL):
        if(save.location == Location.OUTSIDE):
            print('While you are minding your own business outside, you witness a strange creature walking towards you.')
        else:
            print('You hear what sounds like knocking at the airlock of your HAB. At first you think ' \
                'that it\'s a rock hitting the outside airlock, but it\'s too repetitive to be a simple ' \
                'impact from rocks. Besides, the atmosphere outside is too thin to propel a rock large ' \
                'enough to produce that sound at the HAB.')
    elif(save.victory):
        print('Congratulations! You managed to reestablish communications with Mission Control and will not die ' \
            'a slow painful death from starvation on the Red Planet. You may still explore the planet if you wish.')
    
    action = input.parse_action()
    processed = False
    
    if(action != None):
        if(len(action) == 1):
            if(action[0] == 'quit'):
                break
            elif(action[0] == 'save'):
                save_file.seek(0)
                pickle.dump(save, save_file)
                print('Game saved.')
                processed = True
            elif(action[0] == 'load'):
                save_file.seek(0)
                save = pickle.load(save_file)
                print('Game loaded.')
                processed = True
            elif(action[0] == 'look around'):
                if(save.location == Location.BEDROOM):
                    print('You look around at your room. You see your unmade bed, your personal effects, ' \
                      'and the door to the hub of the HAB.')
                elif(save.location == Location.HUB):
                    msg = 'You see the airlock, the kitchen, more terminals, and your EVA-suit. In addition to ' \
                            'the overhead lights, you see red-tinted light streaming through the five-inch thick ' \
                            'glass of the window.'
                    if(save.rover_state == RoverState.InHAB):
                        msg += 'In the center of the room is the Curiosity Rover.'
                    print(msg)
                elif(save.location == Location.OUTSIDE):
                    print('You see the red martian soil all around you. It streches for miles upon miles with no ' \
                        'end in sight. It dawns on you how little you really are on this foreign planet.')
                elif(save.location == Location.ROVER):
                    if(save.rover_state == RoverState.InHAB):
                        print('You see the red martian soil all around you. Behind you is your HAB. In front of you is ' \
                        'where you found the Curiosity Rover. You can see the imprint of its tire treads faintly in ' \
                        'the red soil.')
                    elif(save.rover_state == RoverState.Pushing):
                        print('You see the red martian soil all around. Directly in front of you is the Curiosity Rover that ' \
                        'you\'re slowing pushing.')
                    else:
                        print('You see the Curiosity Rover! With it, you can probably reestablish communications with ' \
                        'Mission Control!')
                processed = True       
        elif(len(action) == 2):
            if(save.location == Location.BEDROOM):
                if(action[0] == 'look at'):
                    if(action[1] == 'terminal'):
                        print('It still reads "CONNECTION LOST".')
                        processed = True
                    elif(action[1] == 'bed'):
                        if(save.bed_made):
                            print('Your bed is nice and neat.')
                        else:
                            print('Your bed is a dissheveled mess. You really need to clean it up.')
                        processed = True
                elif(action[0] == 'make'):
                    if(action[1] == 'bed'):
                        if(save.bed_made):
                            print('You remake your bed. Maybe one of the pillows was out of place. ' \
                                  'Anyway, it was pointless. Your bed was already made.')
                        else:
                            save.bed_made = True
                            print('You make your bed. You pat yourself on the back for such a simple, ' \
                              'meaningless task. At least your mother would be proud.')
                        processed = True
                elif(action[0] == 'go through'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('You leave your bedroom and enter the hub. You see the airlock, the kitchen, your ' \
                                'EVA-suit and more terminals. In addition to the overhead lights, you see red-tinted '\
                                'light streaming through the five-inch thick glass of the windows.')
                            save.location = Location.HUB
                        else:
                            print('You try to go through the door, but the door is closed.')
                        processed = True
                elif(action[0] == 'open'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('The door is already open.')
                        else:
                            print('You open the door to the hub.')
                            save.door_open = True
                        processed = True
                elif(action[0] == 'close'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('You close the door to the hub.')
                            save.door_open = False
                        else:
                            print('The door is already closed.')
                        processed = True
                elif(action[0] == 'pick up' or action[0] == 'take'):
                    if(action[1] == 'book' and not save.book):
                        save.book = True
                        print('You pick up your untouched copy of The Martian Language for Dummies.')
                        processed = True
            elif(save.location == Location.HUB):
                if(action[0] == 'look at'):
                    if(action[1] == 'window'):
                        if(save.turns < 5):
                            print('You look out the window. You see nothing but the red sand. You can tell why' \
                                'it has been dubbed the red planet. In the distance you can make out amorphous ' \
                                'objects heading towards the HAB. It\'s probably the beginning of another sand ' \
                                'storm. You hope not. Sand storms on Mars can last for weeks.')
                        processed = True
                    elif(action[1] == 'kitchen'):
                        print('You look at the HAB\'s minimalistic kitchen. It contains only the essentials: a '\
                            'microwave, a refrigerator, and a table with a chair.' )
                        processed = True
                    elif(action[1] == 'airlock'):
                        print('You look at the airlock. If you got your EVA-suit on, you could go through it and ' \
                            'take a nice walk on the martian surface.')
                        processed = True
                    elif(action[1] == 'terminals'):
                        print('All of the terminals read "CONNECTION LOST." Looks like the connection to Mission ' \
                            'was indeed entirely lost. You are the loneliest person alive. There\'s no other humans ' \
                            'for 54.6 million kilometers.')
                        processed = True
                    elif(action[1] == 'refrigerator'):
                        print('You look at the refrigerator. It\'s still there and faithfully holding all your food.')
                        processed = True
                    elif(action[1] == 'spacesuit' or action[1] == 'eva-suit'):
                        if(not save.eva_suit):
                            print('Your EVA-suit is hanging on the wall. It is a full-body suit complete with a helmet.')
                            processed = True     
                    elif(action[1] == 'photobook'):
                        if(save.need_photo):
                            print('You pick up the photobook and skim through it. There is a photo of the Curiosity Rover! ' \
                                'You take the photo and put it into your pocket to show to the alien.')
                            save.photo = True
                            save.need_photo = False
                        else:
                            msg = 'You pick up the photobook and flip through the pages. There are pictures of Mars, pictures '\
                                'of previous missions to Mars, the Mars rovers, and a lot of photos your family back home. This is ' \
                                'is your old photobook that you would put pictures of Mars in when you were dreaming about becoming ' \
                                'an astronaut. Before you left, you filled up the remaining pages with your family and fond memories.'
                            if(save.photo):
                                msg += ' One of the pictures of the Curiosity Rover is missing.'
                            print(msg)
                        processed = True
                elif(action[0] == 'make'):
                    if(action[1] == 'food'):
                        if(save.food == FoodState.NOTHING):
                            print('You walk over to the kitchen and prepare yourself one of the microwavable meals ' \
                                'from the refrigerator. You twiddle your thumbs for a minute while you wait for the ' \
                                'microwave to finish cooking your food. DING! Your food is ready. You take it out of ' \
                                'the microwave and place it on the table.')
                            save.food = FoodState.MADE
                        elif(save.food == FoodState.MADE):
                            print('You walk over the kitchen and open the fridge before you remember that you had already ' \
                            'made yourself some food and it\'s waiting for you on the counter. You also remember that you ' \
                            'need to ration your food. You close the door of the fridge with a sigh.')
                        else:
                            print('You make it all the way to the frige before you realize that you\'ve already eaten. You aren\'t '\
                            'hungry; you\'re just bored. When you signed up for this mission, you didn\'t realize how boring ' \
                            'it would be.')
                        processed = True
                elif(action[0] == 'eat'):
                    if(action[1] == 'food'):
                        if(save.food == FoodState.NOTHING):
                            print('You look around the kitchen for food that\'s ready to eat, but you can\'t find any. There\'s '\
                            'definitely food in the refrigerator that you could eat.')
                        elif(save.food == FoodState.MADE):
                            print('You eat the food on the table. It\'s your favorite: space food.')
                        else:
                            print('You have already eaten.')
                        processed = True
                elif(action[0] == 'go through'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('You go through the door and enter your bedroom. You see your bed, personal terminal, '\
                            'and personal effects.')
                            save.location = Location.BEDROOM
                        else:
                            print('You try to go through the door, but the door is closed.')
                        processed = True
                    elif(action[1] == 'airlock'):
                        go_through_airlock()
                        processed = True
                elif(action[0] == 'open'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('The door is already open.')
                        else:
                            print('You open the door to your bedroom.')
                            save.door_open = True
                        processed = True
                    elif(action[1] == 'window'):
                        print('The window is not able to be opened. Even if you could manage to open it, doing so would ' \
                            'certainly lead to your demise. The HAB would depressurize, and you would be left in the cruel '
                            'martian atmosphere where you will suffocate.')
                        processed = True
                    elif(action[1] == 'airlock'):
                        open_airlock()
                        processed = True
                elif(action[0] == 'close'):
                    if(action[1] == 'door'):
                        if(save.door_open):
                            print('You close the door to your bedroom.')
                            save.door_open = False
                        else:
                            print('The door is already closed.')
                        processed = True
                    elif(action[1] == 'airlock'):
                        if(save.airlock_open):
                            print('You close the airlock.')
                            save.airlock_open = False
                        else:
                            print('You can\'t close a closed airlock')
                        processed = True
                elif(action[0] == 'wear' or action[0] == 'put on'):
                    if(action[1] == 'spacesuit' or action[1] == 'eva-suit'):
                        if(save.eva_suit):
                            print('You\'re already wearing your EVA-suit.')
                        else:
                            print('You carefully put on your EVA-suit.')
                        save.eva_suit = True
                        processed = True
                elif(action[0] == 'remove' or action[0] == 'take off'):
                    if(action[1] == 'spacesuit' or action[1] == 'eva-suit'):
                        if(save.eva_suit):
                            print('You take off your EVA-suit and hang it back up.')
                            save.eva_suit = False
                        else:
                            print('You can\'t take off something you\'re not wearing.')
                        processed = True
                elif(save.rover_state == RoverState.InHAB and action[1] == 'rover' and not save.victory):
                    if(action[0] == 'use' or action[0] == 'hack'):
                        print('You begin to fiddle with the Curiosity Rover and begin to remove the outer panels protecting its ' \
                            'internals from the harsh martian climate. Although you slacked off in learning Martian, you did not '\
                            'slack in learning how the Curiosity Rover could be used for communication should the communications ' \
                            'array become useless. You open up a panel in the wall of the hub and connect a newly revealed wire to' \
                            'a port in the rover. You go to one of the terminals and set-up the HAB to use the rover for its networking, ' \
                            'and as you finish the process, the "CONNECTION LOST" message disappear from the other screens.')
                        processed = True
                        save.victory = True
            elif(save.location == Location.OUTSIDE):
                if(action[0] == 'look at'):
                    if(action[1] == 'hab'):
                        print('You look at the HAB, your home for your time on Mars. It\'s white exterior contrasts with ' \
                            'the red Martian soil.')
                        processed = True
                elif(action[0] == 'go through'):
                    if(action[1] == 'airlock'):
                        go_through_airlock()
                        processed = True
                elif(action[0] == 'open'):
                    if(action[1] == 'airlock'):
                        open_airlock()
                        processed = True
                elif(action[0] == 'close'):
                    if(action[1] == 'airlock'):
                        if(save.airlock_open):
                            print('You press the button on the outside of the HAB near the airlock. The door of the airlock swings close.')
                            save.airlock_open = False
                        else:
                            print('You can\'t close a closed airlock.')
                        processed = True
                elif(save.turns >= MARTIAN_ARRIVAL and (action[0] == 'speak to' or action[0] == 'speak with' or action[0] == 'talk to')):
                    if(action[1] == 'creature' or action[1] == 'alien' or action[1] == 'martian'):
                        talk_to_martian()
                        processed = True
                elif(save.rover_state == RoverState.Found and action[0] == 'go to' and action[1] == 'rover'):
                    print('You happily make your way towards the shape that the alien had pointed out to you. To your joy, it is, in fact ' \
                        'the Curiosity Rover!')
                    save.location = Location.ROVER
                    processed = True
            elif(save.location == Location.ROVER):
                if(action[0] == 'look at' and action[1] == 'rover' and save.rover_state == RoverState.Found):
                    print('You closely inspect the Curiosity Rover. The once white exterior has been covered in red soil. No wonder you ' \
                        'couldn\'t have found it during your earlier adventures on the Martian surface.')
                    processed = True
                elif((action[0] == 'take' or action[0] == 'pick up') and action[1] == 'rover' and save.rover_state == RoverState.Found):
                    print('You cannot pick up the Curiosity Rover. It may be on Mars where there\'s less gravity, but the rover still weighs a lot. ' \
                        'To pick it up on Mars, you would need to be able to deadlift 1473kgs on Earth. Maybe you could push it instead.')
                    processed = True
                elif(action[0] == 'push' and action[1] == 'rover' and save.rover_state == RoverState.Found):
                    print('You situate yourself behind the Curiosity Rover and begin to push. It\'s difficult to push it through the sand, ' \
                        'but you are making slow but sure progress.')
                    save.rover_state = RoverState.Pushing
                    processed = True
                elif(action[0] == 'go to' and action[1] == 'hab'):
                    if(save.rover_state == RoverState.Pushing):
                        print('You slowly push the rover towards the airlock of the HAB.')
                    else:  
                        print('You return to near the airlock of the HAB.')
                    save.location = Location.OUTSIDE
                    processed = True

    if(processed == False):
        print('Invalid command!')
    else:
        save.turns += 1
