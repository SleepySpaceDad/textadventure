class verb:
    def __init__(self, verb, preps):
        self.verb = verb
        self.preps = preps

    def __str__(self):
        return self.verb
    
    def __repr__(self):
        return self.verb

def init_verbs():
    verbs = []

    with open('verbs.txt') as f:
        lines = f.readlines()

        for line in lines:
            words = line.split()
            verbs.append(verb(words[0], words[1:]))

    return verbs

verbs = init_verbs()

def init_nouns():
    nouns = []
    with open('nouns.txt') as f:
        lines = f.readlines()
        
        for line in lines:
            nouns.append(line.replace('\n', ''))
    
    return nouns
            

nouns = init_nouns()

def parse_action():
    string = input().lower()
    words = string.split(' ')
    if(not words):
        return None

    action = []

    cur_word = 0

    for verb in verbs:
        if(verb.verb == words[cur_word]):
            #if the verb has associated prepositions
            if(verb.preps):
                for prep in verb.preps:
                    if(words[cur_word + 1] == prep):
                        action.append(verb.verb + " " + prep)
                        cur_word += 2
                        break
                break
            else:
                action.append(verb.verb)
                cur_word += 1
                break

    if(cur_word < len(words)):
        if(cur_word < len(words) - 1 and words[cur_word] == 'the'):
            cur_word += 1

        for noun in nouns:
            if(noun == words[cur_word]):
                action.append(noun)
                cur_word += 1
                break

    if(cur_word != len(words)):
        return None
    
    if(not action):
        return None

    return action

def parse_dialogue():
    dialogue = input().lower()
    return dialogue.rstrip('!.?')